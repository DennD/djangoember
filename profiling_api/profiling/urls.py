from django.conf.urls import include, url
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework import routers
from .views import UserProfileView, DetailsView


urlpatterns = [

    url(r'^userprofiles/$', UserProfileView.as_view(), name="create"),
    url(r'^userprofiles/(?P<pk>[0-9]+)/$', DetailsView.as_view(), name="details"),
    
]

urlpatterns = format_suffix_patterns(urlpatterns)

