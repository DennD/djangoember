

from .models import UserProfile
from rest_framework import serializers

class UserProfileSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserProfile
        fields = ('first_name','last_name','in_game_name', 'email','normal_wins',
                'ranked_wins','normal_loss','ranked_loss')
        read_only_fields = ('date_created',)

