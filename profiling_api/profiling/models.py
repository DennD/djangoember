# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class UserProfile(models.Model):
    
    email = models.ForeignKey(User, on_delete=models.CASCADE)
    first_name = models.CharField(max_length = 250, blank = True)
    last_name = models.CharField(max_length = 250, blank = True)
    in_game_name = models.CharField(max_length = 50, blank = True)
    date_created = models.DateField(auto_now=True)
    # #this part should be a different db
    normal_wins = models.IntegerField(default=0)
    ranked_wins = models.IntegerField(default=0)
    normal_loss = models.IntegerField(default=0)
    ranked_loss = models.IntegerField(default=0)
    #active_user = models.BooleanField(default=False) 

    def __str__(self):
        return "{}".format(self.in_game_name)

    class JSONAPIMeta:
        resource_name= "users"

# class Games(models.Model):

#     rank = models.ForeignKey(in_game_name, on_delete=models.CASCADE)
#     normal_wins = models.IntegerField(default=0)
#     ranked_wins = models.IntegerField(default=0)
#     normal_loss = models.IntegerField(default=0)
#     ranked_loss = models.IntegerField(default=0)

    
    


