# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase
from .models import UserProfile
from rest_framework import status
from rest_framework.test import APIClient
from django.core.urlresolvers import reverse
# Create your tests here.

class ModelTestCase(TestCase):

    def setUp(self):
        self.userprofile_email_id = '1'
        self.userprofile = UserProfile(email_id=self.userprofile_email_id)

    def test_model_to_create_records(self):
        previous_count = UserProfile.objects.all()
        self.userprofile.save()
        new_count = UserProfile.objects.count()
        self.assertNotEqual(previous_count, new_count)
    
    def setUp(self):
        self.client = APIClient()
        self.userprofile_data = {'email_id': '1'}
        self.response = self.client.post(
            reverse('create'),
            self.userprofile_data,
            format = "json"
        )
    
    def test_api_to_create_profile(self):

        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)
    
    def test_to_get_profile(self):

        userprofile = UserProfile.objects.all()
        response = self.client.get(
            reverse('details',
            kwargs={'pk': userprofile.id}), 
            format="json"
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, userprofile)
    
    def test_api_to_update_profile(self):

        change_userprofile = {'email_id': '2'}
        response = self.client.post(
            reverse('details',
            kwargs={'pk': userprofile.id}),
            change_userprofile,
            format="json"
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        

    def test_api_to_delete_profile(self):

        delete_userprofile = UserProfile.objects.get()
        response = self.client.delete(
            reverse('details',
            kwargs={'pk': userprofile.id}),
            delete_userprofile,
            format='json',
            follow=True)
        
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)        

